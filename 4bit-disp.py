#!/usr/bin/env python
import minimalmodbus
import time
import numpy as np 
import sys
import struct

minimalmodbus.BAUDRATE = 9600
DEVICE = "/dev/ttyRS485"
#symlink to /dev/ttyUSB0 or ttyS0 
SLAVE = 2 #display 
# port name, slave address (in decimal)
instrument = minimalmodbus.Instrument(DEVICE, SLAVE)

# write
# Register number, value, number of decimals=0, function code, signed=false
# reg = 0 display signed int16_t (-999..1000)  
#instrument.write_register(0, (-92)&0xffff, 0, 6)
# reg = 4 set decimal point 
#instrument.write_register(4, 0, 0, 6)
#instrument.write_register(0, (22)&0xffff, 0, 6)

#func=0x10 reg=0x70, 6 words, only first two matter (with 4 characters) 
#instrument.write_registers(0x70, [0x622d, 0x3248, 0, 0, 0, 0])

#print 4 chars from cmd line
s = sys.argv[1][0:4]+"XYZXYZXY"
#print s, [int(x) for x in struct.unpack('>HHHHHH', s)]
try:
    instrument.write_registers(0x70, [int(x) for x in struct.unpack('>HHHHHH', s)])
except IOError:
    print("Failed to read from instrument %d\n") % SLAVE
    exit(1)

exit(0)
    
