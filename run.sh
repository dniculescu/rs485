#!/bin/sh

n=0
e1=0
e2=0
while true; do
    if ! ./4bit-disp.py "${n}.${e}-------------"; then
	e1=$(($e1+1))
    fi
    if ! ./ydth01.py; then
	e2=$(($e2+1))
    fi
    n=$(($n+1))
    e=$(echo "(${e1}+${e2})/$n" | bc -l)
    echo "n= $n e= ${e1}+${e2} $e"
    sleep 0.01
done 
