#!/usr/bin/env python
import minimalmodbus
import time
import numpy as np 

minimalmodbus.BAUDRATE = 9600

DEVICE = "/dev/ttyRS485"
SLAVE = 1 # th sensor
# port name, slave address (in decimal)
instrument = minimalmodbus.Instrument(DEVICE, SLAVE)
print instrument

if True:
    # Register number, number of decimals, function code
    #print instrument.read_registers(0, 9)
    try:
        print "hum: %d" % instrument.read_register(0, 0, 3)
        print "tmp: %d" % instrument.read_register(1, 0, 3)
        print "dew: %d" % instrument.read_register(2, 0, 3) 
    except IOError:
        print("Failed to read from instrument %d\n") % SLAVE
        exit(1)
    
    for x in range(3, 9, 2):
        break
        try: 
            w1 = instrument.read_register(x, 0, 3) # hi 16bit word
            w2 = instrument.read_register(x+1, 0, 3) # lo 16bit word
        except IOError:
            print("Failed to read from instrument %d\n") % SLAVE
            exit(1)
       #Little endian 
        datab = np.array([w2 & 0xff, w2 >>8, w1 & 0xff, w1 >> 8], dtype=np.uint8)
        dataf = datab.view(dtype=np.float32)
        print dataf[0] 
        time.sleep(1)
    
exit(0)
